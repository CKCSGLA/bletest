/*
 * AppScheduler.cpp
 *
 *  Created on: 07.05.2019
 *      Author: arthur.buchta
 */

#include "AppScheduler.h"
#include "nrf_log.h"

#define SCHED_MAX_EVENT_DATA_SIZE       APP_TIMER_SCHED_EVENT_DATA_SIZE
#define SCHED_QUEUE_SIZE                10

namespace core {

AppScheduler::AppScheduler() {
	// TODO Auto-generated constructor stub

}

void AppScheduler::init(){
	APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}

void AppScheduler::put(const void * p_event_data, uint16_t event_data_size, app_sched_event_handler_t handler){
	app_sched_event_put(p_event_data, event_data_size, handler);
}

} /* namespace core */
