/*
 * Delay.cpp
 *
 *  Created on: 26.04.2019
 *      Author: arthur.buchta
 */

#include "Delay.h"

namespace core {

Delay::Delay() {
	// TODO Auto-generated constructor stub

}

void Delay::ms(int milliseconds){
	nrf_delay_ms(milliseconds);
}

void Delay::us(int microseconds){
	nrf_delay_us(microseconds);
}

} /* namespace core */
