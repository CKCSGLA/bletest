/*
 * Fstoragecore.h
 *
 *  Created on: 09.02.2019
 *      Author: core_alpha
 */

#ifndef MAIN_FSTORAGECORE_H_
#define MAIN_FSTORAGECORE_H_

#include <stdint.h>

#include "Fstorage_core_shim.h"

namespace core {

/**
 * Cpp wrapper class for Fstorage_core_shim
 */
class Fstorage_core {
public:
	Fstorage_core();

	/**
	 * @brief Initialize Fstorage_core
	 */
	void init();

	/**
	 * @brief read data from address
	 * @param addr to read data from
	 * @param p_dest pointer to write readed data to
	 * @param len length of data to be read
	 * @return error code
	 */
	uint8_t read(uint32_t addr, void * p_dest, uint32_t len);

	/**
	 * @brief write bytes to page
	 * @param[in]   dest        Address in flash memory where to write the data.
	 * @param[in]   p_src       Data to be written.
	 * @param[in]   len         Length of the data (in bytes).
	 * @return error code
	 */
	uint8_t writePage(uint32_t dest, void const * p_src, uint32_t len);

	/**
	 * @brief converts 32bit page into 4 bytes
	 * @param data 32bit page data
	 * @param array converted data
	 */
	void convertPageTo8bitArray(uint32_t  *data, uint8_t array[4]);

	/**
	 * @brief erases pages
	 * @param destination Address of the page to erase.
	 * @param pages number of pages to be erased
	 * @return error code
	 */
	uint8_t erase(uint32_t destination, uint32_t pages);

};

} /* namespace core */

#endif /* MAIN_FSTORAGECORE_H_ */
