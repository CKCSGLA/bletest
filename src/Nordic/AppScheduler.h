/*
 * AppScheduler.h
 *
 *  Created on: 07.05.2019
 *      Author: arthur.buchta
 */

#ifndef SRC_MAIN_APPSCHEDULER_H_
#define SRC_MAIN_APPSCHEDULER_H_

extern "C"{
#include "app_scheduler.h"
#include "app_timer.h"
}

namespace core {

/**
 * App Scheduler cpp wrapper
 */
class AppScheduler {
private:
public:
	AppScheduler();

	/**
	 * Initialize appScheduler
	 */
	void init();

	/**
	 * Puts new event into queue
	 * @param p_event_data Data to be committed to event handler
	 * @param event_data_size size of data that will be committed
	 * @param handler event handler
	 */
	void put(const void * p_event_data, uint16_t event_data_size, app_sched_event_handler_t handler);
};

} /* namespace core */

#endif /* SRC_MAIN_APPSCHEDULER_H_ */
