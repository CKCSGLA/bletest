/*
 * Delay.h
 *
 *  Created on: 26.04.2019
 *      Author: arthur.buchta
 */

#ifndef SRC_MAIN_DELAY_H_
#define SRC_MAIN_DELAY_H_
#include "nrf_delay.h"

namespace core {

/**
 * Wrapper class for nrf delay
 */
class Delay {
public:
	Delay();
	/**
	 * @brief delay in ms
	 * @param milliseconds
	 */
	static void ms(int milliseconds);

	/**
	 * @brief delay in microseconds
	 * @param milliseconds
	 */
	static void us(int milliseconds);
};

} /* namespace core */

#endif /* SRC_MAIN_DELAY_H_ */
