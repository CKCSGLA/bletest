#include "Nordicstandard.h"
#include "io/bleIoDevice.h"

core::Nordic_standard norstd;

int main(void)
{

	norstd.Nordic_log_init();
    norstd.Nordic_power_management_init();
    NRF_LOG_INFO("Starting BLE core sensing peripheral");

    core::io::Ble bleA(nullptr, core::io::Device::FLAG_WRITE | core::io::Device::FLAG_READ);
    core::io::Ble bleB(nullptr, core::io::Device::FLAG_READ);
    
    while(!ble.isConnected());
    
    uint8_t buffer[] = "TEST\n";
    
    bleA.send(buffer, sizeof(buffer));

	NRF_LOG_INFO("INIT FINISHED!")

    for (;;)
    {
    	norstd.Nordic_idle_state_handle();
    }
}

