#include "bleIoDevice.h"

extern "C"
{
#include "nrf_sdh_soc.h" //must be included due to bug in nordic sdk
#include "nrf_sdh_ble.h"
#include "nrf_sdh.h"
#include "nrf_ble_gatt.h"
#include "nrf_log.h"
#include "ble_advdata.h"
}

using namespace core::io;

NRF_SDH_BLE_OBSERVER(m_ble_observer, 3, Ble::eventHanlder, NULL);
NRF_BLE_GATT_DEF(m_gatt);
BLE_ADVERTISING_DEF(m_advertising);

Ble::Ble(void (*dataCallback)(const uint8_t* data, const uint16_t len, void* usrData), uint8_t flags, uint8_t* error, void* userData):Device(dataCallback, flags, error, userData)
{
    if(instanceCounter == 0)
    {
        baseUuid = BASE_UUID;
        connectionHandle = BLE_CONN_HANDLE_INVALID;
        serviceHandle    = 0;
        instanceCounter = 0;
        bleInstances[0] = this;
        avUuid.uuid = baseUuid.uuid128[15] | (baseUuid.uuid128[14] << 8);
        avUuid.type = BLE_UUID_TYPE_VENDOR_BEGIN;
        if(!newMainInstance() && error != nullptr) *error = Device::ERROR_RESOURCE_UNAVAIL;
    }
    else if(instanceCounter < maximumInstances)
    {
         bleInstances[instanceCounter] = this;
        if(!newSecondaryInstance()) if(error != nullptr) *error = Device::ERROR_RESOURCE_UNAVAIL;
        else ++instanceCounter;
        
    }
    else if ( error != nullptr ) *error = Device::ERROR_RESOURCE_UNAVAIL;
    if( error != nullptr )*error = Device::NO_ERROR;
}

Ble::~Ble()
{
}

bool Ble::isConnected()
{
    return connectionHandle != BLE_CONN_HANDLE_INVALID;
}

bool Ble::newMainInstance()
{
    bool error = false;
    error |= initBle();
    error |= initGatt();
    error |= initAdvertising();
    error |= initSerivce();
    error |= initConnParams();
    error |= addCharactersitic();
    return !error;
}

bool Ble::newSecondaryInstance()
{
    return addCharactersitic();
}

bool Ble::send(uint8_t* data, uint16_t len, uint8_t* error)
{
    ble_gatts_hvx_params_t     hvx_params;

    memset(&hvx_params, 0, sizeof(hvx_params));
    hvx_params.handle = characitsticHandles.value_handle;
    hvx_params.p_data = data;
    hvx_params.p_len  = &len;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
    
    if(!(flags_ & FLAG_WRITE))
    {
        if(error != nullptr) *error = ERROR_READ_ONLY;
        return false;
    }
    if(!isConnected()) 
    {
        if(error != nullptr) *error = ERROR_NOT_CONNECTED;
        return false;
    }
    if(sd_ble_gatts_hvx(connectionHandle, &hvx_params) != NRF_SUCCESS)
    {
        if(error != nullptr) *error = ERROR_GENERAL;
        return false;
    }
    if(error != nullptr) *error = NO_ERROR;
    return true;
}

void Ble::disconnect()
{
    sd_ble_gap_disconnect(connectionHandle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION); //figure out what dcon type to use
}

void Ble::dataHanlder(ble_evt_t const * p_ble_evt)
{
    const ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
    for(uint8_t i = 0; i < instanceCounter; ++i) if(p_evt_write->handle == bleInstances[i]->characitsticHandles.value_handle)
    {
        bleInstances[i]->dataCallback_(p_evt_write->data, p_evt_write->len, bleInstances[i]->usrData_);
    }
}

void Ble::eventHanlder(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t err_code = NRF_SUCCESS;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected.");
            break;

        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected.");
            connectionHandle = p_ble_evt->evt.gap_evt.conn_handle;
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .tx_phys = BLE_GAP_PHY_AUTO,
                .rx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            break;
            
        case BLE_GATTS_EVT_WRITE:
            dataHanlder(p_ble_evt);
        break;

        default:
            // No implementation needed.
            break;
    }
}


bool Ble::initBle()
{
    ret_code_t err_code = 0;

    err_code += nrf_sdh_enable_request();

    uint32_t ram_start = 0;
    err_code += nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);

    // Enable BLE stack.
    err_code += nrf_sdh_ble_enable(&ram_start);
    
    return !err_code;
}

bool Ble::initGatt()
{
    ret_code_t              err_code = 0;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code += sd_ble_gap_device_name_set(&sec_mode, (const uint8_t *)DEVICE_NAME, strlen(DEVICE_NAME));

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code += sd_ble_gap_ppcp_set(&gap_conn_params);
    
    err_code += nrf_ble_gatt_init(&m_gatt, NULL);
    
    return !err_code;
}

bool Ble::addCharactersitic()
{
    uint32_t            err_code = 0;
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&char_md, 0, sizeof(char_md));
    memset(&attr_md, 0, sizeof(attr_md));
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    char_md.char_props.read   = (bool)(flags_ & FLAG_READ);
    char_md.char_props.write  = (bool)(flags_ & FLAG_WRITE);
    char_md.char_props.notify = 1; 
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL; 
    char_md.p_sccd_md         = NULL;
    
    attr_md.read_perm  = securityMode;
    attr_md.write_perm = securityMode;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;

    ble_uuid.type = BLE_UUID_TYPE_BLE;
    ble_uuid.uuid = serviceUuid+instanceCounter+1;

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = sizeof(uint8_t);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = sizeof(uint8_t);

    err_code = sd_ble_gatts_characteristic_add(serviceHandle, &char_md, &attr_char_value, &characitsticHandles);
    return err_code == NRF_SUCCESS;
}

void Ble::advEventHanlder(ble_adv_evt_t ble_adv_evt)
{}

bool Ble::initAdvertising()
{
    ret_code_t             err_code = 0;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = true;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_complete.uuid_cnt = 1;
    init.advdata.uuids_complete.p_uuids  = &avUuid;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;

    init.evt_handler = advEventHanlder;

    err_code = ble_advertising_init(&m_advertising, &init);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
    return err_code == NRF_SUCCESS;
}

bool Ble::initSerivce()
{
    ret_code_t err_code = 0;

    err_code += sd_ble_uuid_vs_add(&baseUuid, BLE_UUID_TYPE_BLE);
    
    err_code += sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &avUuid, &serviceHandle);
    return !err_code;
}

void Ble::conParamHanler(ble_conn_params_evt_t * p_evt)
{
    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        sd_ble_gap_disconnect(connectionHandle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
    }
}

bool Ble::initConnParams()
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = conParamHanler;
    cp_init.error_handler                  = NULL;

    err_code = ble_conn_params_init(&cp_init);
    return err_code == NRF_SUCCESS;
}
