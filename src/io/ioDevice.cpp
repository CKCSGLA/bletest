#include "ioDevice.h"

using namespace core::io;

Device::Device(void (*dataCallback)(const uint8_t* data, const uint16_t len, void* usrData), uint8_t flags, uint8_t* error, void * const userData): 
usrData_(userData), dataCallback_(dataCallback), flags_(flags)
{}

Device::~Device()
{
    disconnect();
}

void Device::disconnect()
{}


bool Device::isConnected()
{
    return false;
}

bool Device::send(uint8_t octet, uint8_t* error)
{
    return send(&octet, 1, error);
}

uint_fast16_t Device::getMaxDataSize()
{
    return UINT16_MAX;
}
