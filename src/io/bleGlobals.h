#ifndef IO_BLE_GLOBALS_H__
#define IO_BLE_GLOBALS_H__

extern "C"
{

#include "nrf_ble_gatt.h"
#include "ble_advertising.h"

extern nrf_ble_gatt_t m_gatt;
extern ble_advertising_t m_advertising;

    
}

#endif
