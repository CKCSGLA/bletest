#ifndef IO_DEVICE_H__
#define IO_DEVICE_H__

#include <stdint.h>

namespace core::io
{
        class Device
        {
        public:
            
            static constexpr uint8_t NO_ERROR = 0;
            static constexpr uint8_t ERROR_MAXLEN = 1;
            static constexpr uint8_t ERROR_RESOURCE_UNAVAIL = 2;
            static constexpr uint8_t ERROR_NOT_CONNECTED    = 3;
            static constexpr uint8_t ERROR_READ_ONLY        = 4;
            static constexpr uint8_t ERROR_GENERAL          = 255;
            
            static constexpr uint8_t FLAG_READ  = 0b01;
            static constexpr uint8_t FLAG_WRITE = 0b10;
            
        protected:
            
            void * const usrData_;
            
            void (* const dataCallback_)(const uint8_t* data, const uint16_t len, void* usrData);
            
            const uint8_t flags_;
            
        public:
            
            Device(void (*dataCallback)(const uint8_t* data, const uint16_t len, void* usrData), uint8_t flags, uint8_t* error = nullptr, void* userData = nullptr);
            virtual ~Device();
            
            virtual void disconnect();
            
            virtual uint_fast16_t getMaxDataSize();
            
            virtual bool isConnected();
            
            virtual bool send(uint8_t* data, uint16_t len, uint8_t* error = nullptr) = 0;
            
            virtual bool send(uint8_t octet, uint8_t* error = nullptr);
        };
}

#endif
