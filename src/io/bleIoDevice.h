#ifndef IO_BLE_H__
#define IO_BLE_H__

#include <stdint.h>
#include "ioDevice.h"

extern "C"
{
    #include "ble_gatts.h"
    #include "ble.h"
    #include "ble_conn_state.h"
    #include "ble_conn_params.h"
    #include "ble_advertising.h"
    #include "app_timer.h"
}

#define BASE_UUID {{0x9D, 0xB0, 0x13, 0x58, 0x78, 0x55, 0xA9, 0xD1, 0x85, 0x43, 0x66, 0xB6, 0x01, 0x30, 0x42, 0x67}} 

namespace core::io
{
    class Ble: public Device
    {
    private:
        
        //constants
        static constexpr int maximumInstances = 16;
        static constexpr char DEVICE_NAME[] = "TestDev";
        static constexpr char MANUFACTURER_NAME[] = "TestDevMnf";
        static constexpr int APP_BLE_OBSERVER_PRIO = 3;        
        static constexpr int APP_ADV_INTERVAL = 300;
        static constexpr int APP_ADV_DURATION = 18000;
        static constexpr unsigned long serviceUuid = 0x01;
        static constexpr int APP_BLE_CONN_CFG_TAG = 0x01;
        static constexpr int MIN_CONN_INTERVAL = MSEC_TO_UNITS(100, UNIT_1_25_MS); //todo figure out what "units" are. clock ticks?
        static constexpr int MAX_CONN_INTERVAL = MSEC_TO_UNITS(200, UNIT_1_25_MS);
        static constexpr int SLAVE_LATENCY = 0; //?
        static constexpr int CONN_SUP_TIMEOUT = MSEC_TO_UNITS(4000, UNIT_10_MS);
        static constexpr ble_gap_conn_sec_mode_t securityMode = {1,1}; //open link. change this for public version.
        static constexpr int FIRST_CONN_PARAMS_UPDATE_DELAY = APP_TIMER_TICKS(5000);
        static constexpr int NEXT_CONN_PARAMS_UPDATE_DELAY  = APP_TIMER_TICKS(30000);
        static constexpr int MAX_CONN_PARAMS_UPDATE_COUNT = 3;
 
        //statics for main instance
        static inline ble_uuid128_t baseUuid;
        static inline ble_uuid_t avUuid;
        static inline Ble* bleInstances[maximumInstances] = {0};
        static inline uint16_t connectionHandle;
        static inline uint16_t serviceHandle;
        static inline uint8_t instanceCounter = 0;
        
        //variables for secondary instances
        ble_gatts_char_handles_t  characitsticHandles;
        
        //static functions
        static void dataHanlder(ble_evt_t const * p_ble_evt); 
        static void advEventHanlder(ble_adv_evt_t ble_adv_evt);
        static void conParamHanler(ble_conn_params_evt_t * p_evt);
        static bool initBle();
        static bool initGatt();
        static bool initAdvertising();
        static bool initSerivce();
        static bool initConnParams();
        
        //functions
        bool newMainInstance();
        bool newSecondaryInstance();
        bool addCharactersitic();
        
    public:
        
        static void eventHanlder(ble_evt_t const * p_ble_evt, void * p_context);
            
        Ble(void (*dataCallback)(const uint8_t* data, const uint16_t len, void* usrData), uint8_t flags, uint8_t* error = nullptr, void* userData = nullptr);
        virtual ~Ble();
        
        virtual void disconnect();
        
        virtual bool isConnected();
        
        virtual bool send(uint8_t* data, uint16_t len, uint8_t* error = nullptr);
    };
}

#endif

