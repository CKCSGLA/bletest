/*
 * BasicConfiguration.h
 *
 *  Created on: 23.07.2019
 *      Author: User
 */

#ifndef CONFIG_BASICCONFIGURATION_H_
#define CONFIG_BASICCONFIGURATION_H_
#include "stdint.h"
static const uint32_t CORE_FIRMWARE_VERSION	= ((uint32_t) 2003000);
#define CUSTOM_BASE_UUID                  {{0x15, 0x12, 0x8a, 0x76, 0x04, 0xd1, 0x6c, 0x4f, 0x7e, 0x53, 0xf2, 0xe8, 0x00, 0x00, 0xb1, 0x19}} /**< Used vendor specific UUID. */


#endif /* CONFIG_BASICCONFIGURATION_H_ */
